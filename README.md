Решение задачи о ночном клубе
-------------------

Описать происходящее в ночном клубе.

Вводная:

Есть несколько типов музыки: Поп, Рок, Джаз.

Есть несколько типов людей: Мед. работники, Айтишники, Военные, Продавцы.

Каждые 10 секунд меняется тип музыки.

Во время поп-музыки мед.работники и продавцы начинают танцевать или подпевать, айтишники идут в туалет или на улицу, 
военные начинают пить или курить
Во время рок музыки айтишники и военные начинают подпевать и пить, мед.работники идут на улицу, продавцы в туалет
Во время джаза все кроме айтишников начинают танцевать и говорить "о былом", айтишники постоянно просят сменить песню

В клубе находится 20 человек. Тип людей и имена присваиваются рандомно на начальном этапе.
Необходимо при смене типа музыки выводить на экран тип текущей музыки, имя человека, тип человека и чем он занят сейчас.

Нужно написать архитектурно продуманный код, чтобы мы легко могли добавить тип музыки. А при поступлении новых условий не переписывать
 весь код.

Архитектурные решения
-------------------

- Частичное использование DDD подхода, где в папке `domain` находятся основные независимые классы и интерфейсы, которые описывают основную абстрактную модель.
`Visitor` - Посетитель ночного клуба, `VisitorFileBehaviorInterface` - его реакция на файл. Также имеется `DJInterface`, `Player` и `PlayCallbackInterface` для описания влияния смены музыки на `DanceFloor`, где находятся объекты класса `Visitor`

- `domain` делает возможным его расширения таким образом, чтобы решить задачу, но не знает её подробностей

- `services` содержит имплементацию и расширение `domain` для выполнения задачи. Созадётся dj, меняющий песню раз в 10 секунд, создаётся понятие гражданина `Citizen`, который обладает профессией, на основании которой можно создавать поведения для `Visitor`, создаётся callback для смены музыки, реагирующий только на смену жанра.

- `domain` и `services` получились фреймворконезависимыми, поэтому могут быть встроены в любой проект на любом фреймворке, при наличии php >= 7.1.0

- Динамически имплементация и сборка происходит при помощи `Dependency Injection` (файл `config/di.php`), что позволяет передавать в конструктор каждого поведения любые сервисы для их реализации

- Конфигурация происходит в `config/params.php`. Редактирование конфигурации - необходимое и достаточное условие для изменения поведения посетителей, согласно их профессиям и добавления реакции на музыку новых жанров

```
'names' => [
        'Anna',
        'Anton',
        'Victor',
        'Julia',
        'Vyacheslav',
        'Valentine',
        'Ivan',
        'Natalia',
        'Oleg',
    ],
    'prophecyMusicGenreBehaviors' => [
        'programmer' => [
            'pop' => [
                [ //or
                    'app\services\nightclub\behaviors\GoingToTheToiletBehavior',
                    'app\services\nightclub\behaviors\GoingOutBehavior',
                ]
            ],
            'rock' => [
                'app\services\nightclub\behaviors\SingBehavior',
                'app\services\nightclub\behaviors\DrinkBehavior',
            ],
            'jaz' => [
                'app\services\nightclub\behaviors\AskToChangeSongBehavior',
            ],
        ],
        'medic' => [
            'pop' => [
                [ //or
                    'app\services\nightclub\behaviors\DanceBehavior',
                    'app\services\nightclub\behaviors\SingBehavior',
                ],
            ],
            'rock' => [
                'app\services\nightclub\behaviors\GoingOutBehavior',
            ],
            'jaz' => [
                'app\services\nightclub\behaviors\DanceBehavior',
                'app\services\nightclub\behaviors\TalkBehavior',
            ],
        ],
        'warrior'=> [
            'pop' => [
                [ //or
                    'app\services\nightclub\behaviors\DrinkBehavior',
                    'app\services\nightclub\behaviors\SmokeBehavior',
                ]
            ],
            'rock' => [
                'app\services\nightclub\behaviors\SingBehavior',
                'app\services\nightclub\behaviors\DrinkBehavior',
            ],
            'jaz' => [
                'app\services\nightclub\behaviors\DanceBehavior',
                'app\services\nightclub\behaviors\TalkBehavior',
            ],
        ],
        'seller' => [
            'pop' => [
                [ //or
                    'app\services\nightclub\behaviors\DanceBehavior',
                    'app\services\nightclub\behaviors\SingBehavior',
                ]
            ],
            'rock' => [
                'app\services\nightclub\behaviors\GoingToTheToiletBehavior',
            ],
            'jaz' => [
                'app\services\nightclub\behaviors\DanceBehavior',
                'app\services\nightclub\behaviors\TalkBehavior',
            ],
        ]
    ],
    'djFragmentTime' => 10,
```

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      domain/             contains domain classes
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      services/           contains domain implementeed servies
      tests/              contains various tests for the basic application
      uml/                contains uml schemes
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

`PHP >= 7.1.0`


INSTALLATION
------------
```
$ git clone https://goran1301@bitbucket.org/goran1301/nightclub.git
$ composer install
```
