<?php

    /** @var array $params */

    $container = Yii::$container;

    $container->setSingleton('app\services\nightclub\infrastructure\Logger', [
        'class' => 'app\services\nightclub\infrastructure\Logger',
    ]);

    $container->set('app\services\nightclub\behaviors\SmokeBehavior', [
        'class' => 'app\services\nightclub\behaviors\SmokeBehavior',
    ]);

    $container->set('app\services\nightclub\behaviors\DrinkBehavior', [
        'class' => 'app\services\nightclub\behaviors\DrinkBehavior',
    ]);

    $container->set('app\services\nightclub\behaviors\GoingToTheToiletBehavior', [
        'class' => 'app\services\nightclub\behaviors\GoingToTheToiletBehavior',
    ]);

    $container->set('app\services\nightclub\behaviors\GoingOutBehavior', [
        'class' => 'app\services\nightclub\behaviors\GoingOutBehavior',
    ]);

    $container->set('app\services\nightclub\behaviors\SingBehavior', [
        'class' => 'app\services\nightclub\behaviors\SingBehavior',
    ]);

    $container->set('app\services\nightclub\behaviors\AskToChangeSongBehavior', [
        'class' => 'app\services\nightclub\behaviors\AskToChangeSongBehavior',
    ]);

    $container->set('app\services\nightclub\behaviors\DanceBehavior', [
        'class' => 'app\services\nightclub\behaviors\DanceBehavior',
    ]);

    $container->set('app\services\nightclub\behaviors\TalkBehavior', [
        'class' => 'app\services\nightclub\behaviors\TalkBehavior',
    ]);

    $container->set('app\services\nightclub\behaviors\VariableMusicGenreBehavior', [
        'class' => 'app\services\nightclub\behaviors\VariableMusicGenreBehavior',
    ]);

    $container->setSingleton('app\services\factories\VisitorFactory', function () use ($params, $container) {
        $prophecyMusicGenreBehaviors = [];
        foreach ($params['prophecyMusicGenreBehaviors'] as $prophecyName => $prophecyConfig) {
            foreach ($prophecyConfig as $genre => $genreConfig) {
                foreach ($genreConfig as $behaviorInstance) {

                    if (is_array($behaviorInstance)) {
                        $behavior = $container->get('app\services\nightclub\behaviors\VariableMusicGenreBehavior');
                        $behavior->setGenre($genre);
                        $behavior->setBehaviors(array_map(function (string $instance) use ($container) {
                            return $container->get($instance);
                        }, $behaviorInstance));

                    } else {
                        $behavior = $container->get($behaviorInstance);
                        $behavior->setGenre($genre);
                    }

                    $prophecyMusicGenreBehaviors[$prophecyName][] = $behavior;

                }
            }
        }

        return new \app\services\factories\VisitorFactory($prophecyMusicGenreBehaviors);
    });

    $container->setSingleton('app\services\factories\CitizenFactory', function () use($params) {
        return new \app\services\factories\CitizenFactory($params['names'], array_keys($params['prophecyMusicGenreBehaviors']));
    });

    $container->setSingleton('app\domain\nightclub\infrastructure\DanceFloor', [
        'class' => 'app\domain\nightclub\infrastructure\DanceFloor',
        '__construct()' => [[]],
    ]);

    $container->set('app\domain\media\controllers\callback\PlayCallbackInterface', [
        'class' => 'app\services\media\controllers\MusicReactionCallback',
        '__construct()' => [
            \yii\di\Instance::of('app\domain\nightclub\infrastructure\DanceFloor'),
            \yii\di\Instance::of('app\services\nightclub\infrastructure\Logger'),
        ],
    ]);

    $container->set('app\domain\media\controllers\Player', [
        'class' => 'app\domain\media\controllers\Player',
        '__construct()' => [
            [],
            \yii\di\Instance::of('app\domain\media\controllers\callback\PlayCallbackInterface'),
        ],
    ]);

    $container->set('app\domain\nightclub\staff\DJInterface', [
        'class' => 'app\services\nightclub\staff\PlayAllSongsByFragmentsDJ',
        '__construct()' => [$params['djFragmentTime']],
    ]);

    $container->setSingleton('app\domain\nightclub\NightClub', [
        'class' => 'app\domain\nightclub\NightClub',
        '__construct()' => [
            \yii\di\Instance::of('app\domain\media\controllers\Player'),
            \yii\di\Instance::of('app\domain\nightclub\staff\DJInterface'),
            \yii\di\Instance::of('app\domain\nightclub\infrastructure\DanceFloor'),
        ],
    ]);


