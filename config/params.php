<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'names' => [
        'Anna',
        'Anton',
        'Victor',
        'Julia',
        'Vyacheslav',
        'Valentine',
        'Ivan',
        'Natalia',
        'Oleg',
    ],
    'prophecyMusicGenreBehaviors' => [
        'programmer' => [
            'pop' => [
                [ //or
                    'app\services\nightclub\behaviors\GoingToTheToiletBehavior',
                    'app\services\nightclub\behaviors\GoingOutBehavior',
                ]
            ],
            'rock' => [
                'app\services\nightclub\behaviors\SingBehavior',
                'app\services\nightclub\behaviors\DrinkBehavior',
            ],
            'jaz' => [
                'app\services\nightclub\behaviors\AskToChangeSongBehavior',
            ],
        ],
        'medic' => [
            'pop' => [
                [ //or
                    'app\services\nightclub\behaviors\DanceBehavior',
                    'app\services\nightclub\behaviors\SingBehavior',
                ],
            ],
            'rock' => [
                'app\services\nightclub\behaviors\GoingOutBehavior',
            ],
            'jaz' => [
                'app\services\nightclub\behaviors\DanceBehavior',
                'app\services\nightclub\behaviors\TalkBehavior',
            ],
        ],
        'warrior'=> [
            'pop' => [
                [ //or
                    'app\services\nightclub\behaviors\DrinkBehavior',
                    'app\services\nightclub\behaviors\SmokeBehavior',
                ]
            ],
            'rock' => [
                'app\services\nightclub\behaviors\SingBehavior',
                'app\services\nightclub\behaviors\DrinkBehavior',
            ],
            'jaz' => [
                'app\services\nightclub\behaviors\DanceBehavior',
                'app\services\nightclub\behaviors\TalkBehavior',
            ],
        ],
        'seller' => [
            'pop' => [
                [ //or
                    'app\services\nightclub\behaviors\DanceBehavior',
                    'app\services\nightclub\behaviors\SingBehavior',
                ]
            ],
            'rock' => [
                'app\services\nightclub\behaviors\GoingToTheToiletBehavior',
            ],
            'jaz' => [
                'app\services\nightclub\behaviors\DanceBehavior',
                'app\services\nightclub\behaviors\TalkBehavior',
            ],
        ]
    ],
    'djFragmentTime' => 10,
];
