<?php

namespace app\controllers;

use app\domain\nightclub\NightClub;
use app\models\forms\NightclubSimulationForm;
use app\services\factories\CitizenFactory;
use app\services\factories\VisitorFactory;
use app\services\media\Song;
use app\services\nightclub\infrastructure\Logger;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var NightClub
     */
    private $nightClub;

    /**
     * @var VisitorFactory
     */
    private $visitorFactory;

    /**
     * @var CitizenFactory
     */
    private $citizenFactory;

    /**
     * SiteController constructor.
     * @param $id
     * @param $module
     * @param Logger $logger
     * @param NightClub $nightClub
     * @param VisitorFactory $visitorFactory
     * @param CitizenFactory $citizenFactory
     * @param array $config
     */
    public function __construct($id, $module, Logger $logger, NightClub $nightClub, VisitorFactory $visitorFactory, CitizenFactory $citizenFactory, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->logger = $logger;
        $this->nightClub = $nightClub;
        $this->visitorFactory = $visitorFactory;
        $this->citizenFactory = $citizenFactory;

    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $nightClubForm = new NightclubSimulationForm();
        $messages = [];

        if ($nightClubForm->load(Yii::$app->request->post()) && $nightClubForm->validate()) {
            foreach ($this->citizenFactory->makeRandomCitizens(20) as $citizen) {
                $this->nightClub->addVisitor($this->visitorFactory->makeVisitor($citizen));
            }
            $this->nightClub->setPlaylist($this->initPlaylist());
            $this->nightClub->update($nightClubForm->duration);
            $messages = $this->logger->getMessages();
        }

        return $this->render('index', [
            'messages' => $messages,
            'nightClubForm' => $nightClubForm,
        ]);
    }

    /**
     * Плейлист
     *
     * @return array
     */
    public function initPlaylist(): array
    {
        return [
            new Song('pop'),
            new Song('rock'),
            new Song('jaz'),
        ];
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
