<?php

namespace app\domain\media\controllers;

use app\domain\media\controllers\callback\PlayCallbackInterface;
use app\domain\media\files\PlayableInterface;

/**
 * Class Player
 *
 * @package app\domain\media\controllers
 *
 * Плеер для воспроизведения файлов
 */
class Player
{
    /**
     * @var PlayableInterface[]
     */
    protected $playlist;

    /**
     * @var PlayCallbackInterface
     */
    protected $playCallback;

    /**
     * @var int|null
     */
    protected $currentFileIndex = null;

    /**
     * Player constructor.
     *
     * @param PlayableInterface[]   $playlist список файлов для воспроизведения
     * @param PlayCallbackInterface $playCallback callback при воспроизведении нового файла
     */
    public function __construct(array $playlist, PlayCallbackInterface $playCallback)
    {
        $this->playlist = $playlist;
        $this->playCallback = $playCallback;
        if (!empty($playlist)) {
            $this->currentFileIndex = 0;
        }
    }

    /**
     * @param PlayCallbackInterface $playCallback
     */
    public function setPlayCallback(PlayCallbackInterface $playCallback): void
    {
        $this->playCallback = $playCallback;
    }

    /**
     * Проигрывать следующий файл
     *
     * @return bool
     */
    public function nextFile(): bool
    {
        $nextFileIndex = $this->nextSongIndex();

        if (isset($this->playlist[$nextFileIndex])) {
            $this->currentFileIndex = $nextFileIndex;
            $this->playlist[$this->currentFileIndex]->play();
            $this->playCallback->callback($this->playlist[$this->currentFileIndex]);
            return true;
        }

        $this->currentFileIndex = null;
        return false;
    }

    /**
     * К началу плейлиста
     */
    public function toTheStart(): void
    {
        $this->currentFileIndex = null;
    }

    /**
     * Индекс следующего файла
     *
     * @return int
     */
    private function nextSongIndex(): int
    {
        if ($this->currentFileIndex === null) {
            return 0;
        }
        return $this->currentFileIndex + 1;
    }

    /**
     * Проигрывается ли какой-либо файл
     *
     * @return bool
     */
    public function isPlaying(): bool
    {
        return $this->currentFileIndex !== null;
    }

    /**
     * @param PlayableInterface[] $playlist
     */
    public function setPlaylist(array $playlist): void
    {
        $this->playlist = $playlist;
        $this->toTheStart();
    }

}