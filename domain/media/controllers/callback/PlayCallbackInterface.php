<?php

namespace app\domain\media\controllers\callback;

use app\domain\media\files\PlayableInterface;

/**
 * Interface PlayCallbackInterface
 *
 * @package app\domain\media\callback
 *
 * callback при старте нового файла в плеере
 */
interface PlayCallbackInterface
{
    /**
     * Действие callback'а
     *
     * @param PlayableInterface $playing
     */
    public function callback(PlayableInterface $playing): void;
}