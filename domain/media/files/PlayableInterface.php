<?php

namespace app\domain\media\files;

/**
 * Interface PlayableInterface
 *
 * @package app\domain\media\files
 *
 * Возможность проигрывания
 */
interface PlayableInterface
{
    /**
     * Всопроизведение файла
     */
    public function play(): void;
}