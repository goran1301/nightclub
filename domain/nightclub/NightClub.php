<?php

namespace app\domain\nightclub;

use app\domain\media\controllers\Player;
use app\domain\nightclub\infrastructure\DanceFloor;
use app\domain\nightclub\staff\DJInterface;

/**
 * Class NightClub
 *
 * @package app\domain\nightclub
 *
 * Ночной клуб, фасад, под которым происходят все процессымежду посетителями, персоналом и инфраструктурой
 */
class NightClub
{
    /**
     * @var Player
     */
    protected $player;

    /**
     * @var DJInterface
     */
    protected $dj;

    /**
     * @var DanceFloor
     */
    protected $danceFloor;

    /**
     * NightClub constructor.
     *
     * @param Player      $player
     * @param DJInterface $dj
     * @param DanceFloor  $danceFloor
     */
    public function __construct(Player $player, DJInterface $dj, DanceFloor $danceFloor)
    {
        $this->player = $player;
        $this->dj = $dj;
        $this->danceFloor = $danceFloor;
    }

    /**
     * Обновление состояния системы на deltaTime секунд
     *
     * @param int $deltaTime
     */
    public function update(int $deltaTime): void
    {
        $this->dj->update($this->player, $deltaTime);
    }

    /**
     * Добавление посетителя на танцпол
     *
     * @param Visitor $visitor
     */
    public function addVisitor(Visitor $visitor): void
    {
        $this->danceFloor->addVisitor($visitor);
    }

    /**
     * Сменить плейлист для плеера
     *
     * @param array $playlist
     */
    public function setPlaylist(array $playlist): void
    {
        $this->player->setPlaylist($playlist);
        $this->dj->startParty($this->player);
    }

}