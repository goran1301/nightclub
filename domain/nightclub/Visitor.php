<?php

namespace app\domain\nightclub;

use app\domain\nightclub\behaviors\VisitorsFileBehaviorInterface;

/**
 * Class Visitor
 *
 * @package app\domain\nightclub
 *
 * Посетитель ночного клуба
 */
class Visitor
{
    /**
     * @var mixed
     */
    private $citizen;

    /**
     * @var VisitorsFileBehaviorInterface[]
     */
    protected $musicBehaviors;

    /**
     * Visitor constructor.
     *
     * @param VisitorsFileBehaviorInterface[] $musicBehaviors
     * @param mixed $citizen гражданин, являющийся посетителем
     */
    public function __construct(iterable $musicBehaviors, $citizen)
    {
        $this->musicBehaviors = $musicBehaviors;
        $this->citizen = $citizen;
    }

    /**
     * @return VisitorsFileBehaviorInterface[]
     */
    public function getMusicBehaviors(): array
    {
        return $this->musicBehaviors;
    }

    /**
     * Только активные поведения
     *
     * @return VisitorsFileBehaviorInterface[]
     */
    public function getActiveBehaviors(): array
    {
        return array_filter($this->musicBehaviors, function (VisitorsFileBehaviorInterface $behavior) {
            return $behavior->isActive();
        });
    }

    /**
     * Прекратить любую активность
     */
    public function stopAllBehaviors(): void
    {
        foreach ($this->musicBehaviors as $behavior) {
            $behavior->stop();
        }
    }

    /**
     * @return mixed
     */
    public function getCitizen()
    {
        return $this->citizen;
    }

}