<?php

namespace app\domain\nightclub\behaviors;

use app\domain\media\files\PlayableInterface;
use app\domain\nightclub\Visitor;

/**
 * Interface VisitorsFileBehaviorInterface
 *
 * @package app\domain\nightclub\behaviors
 *
 * Поведение посетителя во время проигрывания файла
 */
interface VisitorsFileBehaviorInterface
{
    /**
     * Поведение посетителя во время проигрывания файла
     *
     * @param Visitor $visitor посетитель, осуществляющий поведение
     * @param PlayableInterface $file файл, для которого проверяется актуальность данного поведения
     */
    public function behave(Visitor $visitor, PlayableInterface $file): void;

    /**
     * Актуально ли поведение для данного файла
     *
     * @param Visitor $visitor посетитель, осуществляющий поведение
     * @param PlayableInterface $file файл, вызывающийповедение
     *
     * @return bool
     */
    public function haveToBehave(Visitor $visitor, PlayableInterface $file): bool;

    /**
     * Выполняется ли поведение
     *
     * @return bool
     */
    public function isActive(): bool;

    /**
     * Прекратить поведение
     */
    public function stop(): void;
}