<?php

namespace app\domain\nightclub\infrastructure;

use app\domain\nightclub\Visitor;

/**
 * Class DanceFloor
 *
 * @package app\domain\nightclub\infrastructure
 *
 * Танцпол, содержащий активных посетителей
 */
class DanceFloor
{
    /**
     * @var Visitor[]
     */
    protected $visitors;

    /**
     * DanceFloor constructor.
     *
     * @param Visitor[] $visitors
     */
    public function __construct(iterable $visitors)
    {
        $this->visitors = $visitors;
    }

    /**
     * @return Visitor[]
     */
    public function getVisitors(): array
    {
        return $this->visitors;
    }

    /**
     * Добавление посетителя
     *
     * @param Visitor $visitor
     */
    public function addVisitor(Visitor $visitor): void
    {
        $this->visitors[] = $visitor;
    }

}