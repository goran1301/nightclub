<?php

namespace app\domain\nightclub\staff;

use app\domain\media\controllers\Player;

/**
 * Interface DJInterface
 *
 * @package app\domain\nightclub\staff
 *
 * DJ для управления плеером в ночном клубе
 */
interface DJInterface
{
    /**
     * Действие за deltaTime
     *
     * @param Player $player плеер, которым должен управлять DJ
     * @param int $deltaTime время в секундах, которое прошло с момента предыдущего вызова метода
     */
    public function update(Player $player, int $deltaTime): void;

    /**
     * Запуск плеера и действия в начале вечеринки
     *
     * @param Player $player
     */
    public function startParty(Player $player): void;
}