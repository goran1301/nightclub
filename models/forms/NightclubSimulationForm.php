<?php

namespace app\models\forms;

use yii\base\Model;

/**
 * Class NightclubSimulationForm
 * @package app\models\forms
 *
 * Форма для задания времени симуляции ночного клуба
 */
class NightclubSimulationForm extends Model
{
    /**
     * Время в секундах
     *
     * @var int
     */
    public $duration = 60;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            ['duration', 'integer'],
            ['duration', 'compare', 'compareValue' => 0, 'operator' => '>=', 'type' => 'number'],
        ];
    }
}