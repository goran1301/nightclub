<?php

namespace app\services\factories;

use app\services\models\Citizen;

/**
 * Class CitizenFactory
 *
 * @package app\services\factories
 *
 * Фабрика случайных граждан
 */
class CitizenFactory
{
    /**
     * @var string[]
     */
    private $names = [];

    /**
     * @var string[]
     */
    private $prophecies = [];

    /**
     * CitizenFactory constructor.
     *
     * @param string[] $names
     * @param string[] $prophecies
     */
    public function __construct(array $names, array $prophecies)
    {
        $this->names = $names;
        $this->prophecies = $prophecies;
    }

    /**
     * @return Citizen
     */
    public function makeRandomCitizen(): Citizen
    {
        return new Citizen(
            $this->prophecies[rand(0, count($this->prophecies) - 1)],
            $this->names[rand(0, count($this->names) - 1)]
        );
    }

    /**
     * @param int $count
     * @return Citizen[]
     */
    public function makeRandomCitizens(int $count): array
    {
        $citizens = [];
        for ($i = 0; $i < $count; $i++) {
            $citizens[] = $this->makeRandomCitizen();
        }

        return $citizens;
    }
}