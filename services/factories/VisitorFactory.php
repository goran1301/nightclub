<?php

namespace app\services\factories;

use app\domain\nightclub\Visitor;
use app\services\models\Citizen;

/**
 * Class VisitorFactory
 *
 * @package app\services\factories
 *
 * Фабрика для генерации посетителей ночного клуба
 */
class VisitorFactory
{
    /**
     * @var array
     */
    private $prophecyMusicGenreBehaviors;

    /**
     * VisitorFactory constructor.
     *
     * @param array $prophecyMusicGenreBehaviors
     */
    public function __construct(array $prophecyMusicGenreBehaviors)
    {
        $this->prophecyMusicGenreBehaviors = $prophecyMusicGenreBehaviors;
    }

    /**
     * Создание посетителя
     *
     * @param Citizen $citizen
     *
     * @return Visitor
     */
    public function makeVisitor(Citizen $citizen): Visitor
    {
        return new Visitor(
            $this->prophecyMusicGenreBehaviors[$citizen->getProphecy()] ?? [],
            $citizen
        );
    }
}