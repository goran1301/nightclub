<?php

namespace app\services\media;

use app\domain\media\files\PlayableInterface;

/**
 * Class Song
 *
 * @package app\services\media
 *
 * Песня, как частный случай файла
 */
class Song implements PlayableInterface
{
    /**
     * @var string
     */
    protected $genre;

    /**
     * Song constructor.
     *
     * @param string $genre
     */
    public function __construct(string $genre)
    {
        $this->genre = $genre;
    }

    /**
     * @inheritDoc
     */
    public function play(): void
    {
        return;
    }

    /**
     * @return string
     */
    public function getGenre(): string
    {
        return $this->genre;
    }
}