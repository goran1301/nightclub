<?php

namespace app\services\media\controllers;

use app\domain\media\controllers\callback\PlayCallbackInterface;
use app\domain\media\files\PlayableInterface;
use app\domain\nightclub\infrastructure\DanceFloor;
use app\services\nightclub\infrastructure\Logger;
use app\services\media\Song;

/**
 * Class MusicReactionCallback
 *
 * @package app\services\media\controllers
 *
 * Реакция посетителей на музыку
 */
class MusicReactionCallback implements PlayCallbackInterface
{
    /**
     * @var DanceFloor
     */
    protected $danceFloor;

    /**
     * @var string|null
     */
    private $lastGenre = null;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * MusicReactionCallback constructor.
     *
     * @param DanceFloor $danceFloor
     * @param Logger $logger
     */
    public function __construct(DanceFloor $danceFloor, Logger $logger)
    {
        $this->danceFloor = $danceFloor;
        $this->logger = $logger;
    }

    /**
     * Действие callback'а
     *
     * @param PlayableInterface $playing
     */
    public function callback(PlayableInterface $playing): void
    {
        if (!$playing instanceof Song || $this->lastGenre === $playing->getGenre()) {
            return;
        }
        $this->lastGenre = $playing->getGenre();
        $this->logger->addGenrePlayingMessage($playing->getGenre());

        foreach ($this->danceFloor->getVisitors() as $visitor) {
            foreach ($visitor->getMusicBehaviors() as $key => $musicBehavior) {
                if ($musicBehavior->haveToBehave($visitor, $playing)) {
                    $musicBehavior->behave($visitor, $playing);
                }else {
                    $musicBehavior->stop();
                }
            }

            $this->logger->addGenreBehaviorMessage($visitor->getCitizen(), $visitor->getActiveBehaviors());
        }
    }
}