<?php

namespace app\services\models;

/**
 * Class Citizen
 *
 * @package app\services\models
 */
class Citizen
{
    /**
     * @var string
     */
    protected $prophecy;

    /**
     * @var string
     */
    protected $name;

    /**
     * Citizen constructor.
     *
     * @param string $prophecy
     * @param string $name
     */
    public function __construct(string $prophecy, string $name)
    {
        $this->prophecy = $prophecy;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getProphecy(): string
    {
        return $this->prophecy;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}