<?php

namespace app\services\nightclub\behaviors;

use app\domain\media\files\PlayableInterface;
use app\domain\nightclub\Visitor;

/**
 * Class DanceBehavior
 * @package app\services\nightclub\behaviors
 *
 * Танцевать
 */
class DanceBehavior extends MusicGenreBehavior
{

    /**
     * Выполнение поведения
     *
     * @param Visitor $visitor
     * @param PlayableInterface $song
     */
    protected function behaviorProcession(Visitor $visitor, PlayableInterface $song): void
    {
        // TODO: Implement behaviorProcession() method.
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'dancing';
    }
}