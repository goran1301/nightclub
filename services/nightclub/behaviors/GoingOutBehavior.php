<?php

namespace app\services\nightclub\behaviors;

use app\domain\media\files\PlayableInterface;
use app\domain\nightclub\Visitor;

/**
 * Class GoingOutBehavior
 * @package app\services\nightclub\behaviors
 *
 * Выйти на улицу
 */
class GoingOutBehavior extends MusicGenreBehavior
{

    /**
     * Выполнение поведения
     *
     * @param Visitor $visitor
     * @param PlayableInterface $song
     */
    protected function behaviorProcession(Visitor $visitor, PlayableInterface $song): void
    {
        // TODO: Implement behaviorProcession() method.
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'going out behavior';
    }
}