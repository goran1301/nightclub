<?php

namespace app\services\nightclub\behaviors;

use app\domain\media\files\PlayableInterface;
use app\domain\nightclub\behaviors\VisitorsFileBehaviorInterface;
use app\domain\nightclub\Visitor;
use app\services\media\Song;

/**
 * Class MusicGenreBehavior
 *
 * @package app\services\nightclub\behaviors
 *
 * Поведение посетителя, зависящее от жанра музыки
 */
abstract class MusicGenreBehavior implements VisitorsFileBehaviorInterface
{
    /**
     * @var string
     */
    protected $genre;

    /**
     * @var bool
     */
    private $isActive = false;

    /**
     * Актуально ли поведение для данного файла
     *
     * @param Visitor $visitor посетитель, осуществляющий поведение
     * @param PlayableInterface $file файл, вызывающийповедение
     *
     * @return bool
     */
    public function haveToBehave(Visitor $visitor, PlayableInterface $file): bool
    {
        if ($file instanceof Song) {
            return $this->genre === $file->getGenre();
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function behave(Visitor $visitor, PlayableInterface $file): void
    {
        if ($this->isActive()) {
            return;
        }

        $this->behaviorProcession($visitor, $file);
        $this->isActive = true;
    }

    /**
     * Выполнение поведения
     *
     * @param Visitor $visitor
     * @param PlayableInterface $song
     */
    abstract protected function behaviorProcession(Visitor $visitor, PlayableInterface $song): void;

    /**
     * @return string
     */
    abstract public function __toString();

    /**
     * Выполняется ли поведение
     *
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * Прекратить поведение
     */
    public function stop(): void
    {
        $this->isActive = false;
    }

    /**
     * @param string $genre
     */
    public function setGenre(string $genre): void
    {
        $this->genre = $genre;
    }
}