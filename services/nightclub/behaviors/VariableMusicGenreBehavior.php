<?php

namespace app\services\nightclub\behaviors;

use app\domain\media\files\PlayableInterface;
use app\domain\nightclub\Visitor;

/**
 * Class VariableMusicGenreBehavior
 * @package app\services\nightclub\behaviors
 *
 * Вариативное поведение
 */
class VariableMusicGenreBehavior extends MusicGenreBehavior
{
    /**
     * @var MusicGenreBehavior[]
     */
    private $behaviors;

    /**
     * @var MusicGenreBehavior
     */
    private $selectedBehavior;

    /**
     * Выполнение поведения
     *
     * @param Visitor $visitor
     * @param PlayableInterface $song
     */
    protected function behaviorProcession(Visitor $visitor, PlayableInterface $song): void
    {
        $this->selectBehavior();
        $this->selectedBehavior->behave($visitor, $song);
    }

    /**
     * Выбор одного из поведений
     */
    private function selectBehavior(): void
    {
        $this->selectedBehavior = $this->behaviors[rand(0, count($this->behaviors) - 1)];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->selectedBehavior !== null) {
            return (string) $this->selectedBehavior;
        }

        return implode('or ', $this->behaviors);
    }

    /**
     * @inheritDoc
     */
    public function stop(): void
    {
        $this->selectedBehavior = null;
        parent::stop();
    }

    /**
     * @param MusicGenreBehavior[] $behaviors
     */
    public function setBehaviors(array $behaviors): void
    {
        $this->behaviors = $behaviors;
    }
}