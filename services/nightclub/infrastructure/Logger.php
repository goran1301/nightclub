<?php

namespace app\services\nightclub\infrastructure;

use app\services\models\Citizen;

/**
 * Class Logger
 *
 * @package app\services\infrastructure
 *
 * Логирование происходящего в ночном клубе
 */
class Logger
{
    /**
     * @var Message[]
     */
    private $messages = [];

    /**
     * @return Message[]
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * Очистка лога
     */
    public function clear(): void
    {
        $this->messages = [];
    }

    /**
     * Добавление сообщения в лог
     *
     * @param string $message
     * @param string|null $type
     */
    public function addMessage(string $message, ?string $type = null): void
    {
        $this->messages[] = new Message($message, $type);
    }

    /**
     * @param Citizen $citizen
     * @param string[]  $behaviorDescription
     */
    public function addGenreBehaviorMessage(Citizen $citizen, array $behaviorDescription): void
    {
        $this->addMessage('A visitor '.$citizen->getName().' ('.$citizen->getProphecy().') is '.implode(' and ', $behaviorDescription), 'genre-behavior');
    }

    /**
     * @param string $genre
     */
    public function addGenrePlayingMessage(string $genre): void
    {
        $this->addMessage($genre.' music is now playing', 'genre-plying');
    }
}