<?php

namespace app\services\nightclub\infrastructure;

/**
 * Class Message
 * @package app\services\nightclub\infrastructure
 *
 * Сообщение в логе
 */
class Message
{
    /**
     * @var ?string
     */
    private $type;

    /**
     * @var string
     */
    private $message;

    /**
     * Message constructor.
     * @param string $message
     * @param string|null $type
     */
    public function __construct(string $message, ?string $type)
    {
        $this->message = $message;
        $this->type = $type;
    }

    /**
     * @param string $type
     * @return bool
     */
    public function typeIs(string $type): bool
    {
        return $this->type === $type;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->message;
    }
}