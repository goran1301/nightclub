<?php

namespace app\services\nightclub\staff;

use app\domain\media\controllers\Player;
use app\domain\nightclub\staff\DJInterface;

/**
 * Class PlayAllSongsByFragmentsDJ
 *
 * @package app\services\nightclub\staff
 *
 * DJ, который проигрывает каждую песню по 10 секунд, после чего запускает плейлист снова
 */
class PlayAllSongsByFragmentsDJ implements DJInterface
{
    /**
     * @var int
     */
    private $fragmentTime;

    /**
     * @var int
     */
    private $currentFragmentTimePlayed = 0;

    /**
     * PlayAllSongsByFragmentsDJ constructor.
     *
     * @param int $fragmentTime
     */
    public function __construct(int $fragmentTime)
    {
        $this->fragmentTime = $fragmentTime;
    }

    /**
     * Действие за deltaTime
     *
     * @param Player $player    плеер, которым должен управлять DJ
     * @param int    $deltaTime время в секундах, которое прошло с момента предыдущего вызова метода
     */
    public function update(Player $player, int $deltaTime): void
    {
        $this->currentFragmentTimePlayed += $deltaTime;

        while ($this->currentFragmentTimePlayed >= $this->fragmentTime) {
            $this->playNext($player);
            $this->currentFragmentTimePlayed -= $this->fragmentTime;
        }
    }

    /**
     * Проигрывает следующий трек. Если следующий трек отсутствует - проигрывает первый.
     *
     * @param Player $player
     */
    private function playNext(Player $player): void
    {
        $player->nextFile();
        if (!$player->isPlaying()) {
            $player->toTheStart();
            $player->nextFile();
        }
    }

    /**
     * Запуск плеера и действия в начале вечеринки
     *
     * @param Player $player
     */
    public function startParty(Player $player): void
    {
        $this->currentFragmentTimePlayed = 0;
        $this->playNext($player);
    }
}