<?php

use app\models\forms\NightclubSimulationForm;
use app\services\nightclub\infrastructure\Message;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var $this yii\web\View */
/** @var Message[] $messages */
/** @var NightclubSimulationForm $nightClubForm */

$this->title = 'Night club simulation';
?>
<div>
    <h1>The Nightclub Simulation!</h1>
    <p class="lead">Enter the party duration in seconds to start</p>


    <div class="text-left nightclub-form">
        <?php $form = ActiveForm::begin() ?>
        <?= $form->field($nightClubForm, 'duration') ?>
        <?= Html::submitButton('Run', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end();?>
    </div>
</div>



<div class="row">
    <div class="col-lg-4">
        <?php foreach ($messages as $message):?>

            <?php if($message->typeIs('genre-plying')): ?>
                <h3> <?= $message ?> </h3>
            <?php endif;?>

            <?php if($message->typeIs('genre-behavior')): ?>
                <span class="block"> <?= $message ?> </span>
            <?php endif;?>

        <?php endforeach;?>
    </div>
</div>
